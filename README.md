# Telegram BOT
Для того, чтобы воспользоваться ботом, сначала нужно зарегистрироваться на сайте, указав актуальный никнейм Telegram 

Ссылка на бота: @call_be_bot

1. Linux / Mac
```bash
foo@bar:~$ cd simple
foo@bar:~$ virtualenv env
foo@bar:~$ source env/bin/activate
```

1. Windows
```bash
foo@bar:~$ cd simple
foo@bar:~$ virtualenv env
foo@bar:~$ env/Scripts/activate.ps1
```

2. Установка зависимостей
```bash
foo@bar:~$ pip install requirements.txt
```

3. Получение токена

3.1 Нужно задать переменную окружения 

*Windows*
```bash
set API_TOKEN=string
```

*Linux / Mac*
```bash
export API_TOKEN=string
```
3.2 либо python-файл c названием config.py и переменной API_TOKEN

*Windows*

создать файл __config.py__

*Linux / Mac*
```bash
touch config.py
```

```python
API_TOKEN = ''
```

4. Запуск
```bash
foo@bar:~$ python main.py
```
