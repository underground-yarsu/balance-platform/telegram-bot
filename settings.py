import os
from datetime import timedelta

API_TOKEN = os.getenv('API_TOKEN')
HOST_NAME = os.getenv('HOST_NAME', 'https://back-hack.herokuapp.com')
DELAY = timedelta(days=1).total_seconds()


