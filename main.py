from datetime import datetime, timedelta
from typing import List

import httpx
import logging
import asyncio

from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import Message
from settings import API_TOKEN, DELAY, HOST_NAME

logging.basicConfig(level=logging.INFO)


bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)
loop = asyncio.get_event_loop()
status = ...


@dp.message_handler(commands=['start', 'help'])
async def get_news(message: Message):
    async with httpx.AsyncClient() as client:
        global status
        user = await bot.get_chat_member(message.chat.id, message.from_user.id)
        username = dict(user).get('user').get('username')
        user_status = await client.get(f'{HOST_NAME}/check_user/{username}')
        if user_status.status_code == 409:
            return await message.answer("Сначала нужно зарегистрироваться на сайте")
        achievements_status = await client.get(f'{HOST_NAME}/get_status/{username}')
        status = achievements_status.json().get('status')
    # await get_status()
    await get_notifications(message)
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["Мои достижения", "Все достижения пользователей", "Все достижения команд"]
    keyboard.add(*buttons)
    await message.answer("Привет! Нажми нужную кнопку", reply_markup=keyboard)


@dp.message_handler(lambda message: message.text == "Все достижения пользователей")
async def a_without_puree(message: types.Message):
    async with httpx.AsyncClient() as client:
        achievements_user_result = await client.get(f'{HOST_NAME}/list_achieve_users')
        achievements = achievements_user_result.json()
        answer = parse_json(achievements, 'name_ach', 'ach_price')
        await message.answer(answer, parse_mode='Markdown')


def parse_json(achievements: List[dict], name_key: str, price_key: str):
    answer = ''
    for ach in achievements:
        answer += f"_{ach[name_key]}_\t"
        answer += f"*{ach[price_key]}*\n"
    return answer


@dp.message_handler(lambda message: message.text == "Мои достижения")
async def without_puree(message: types.Message):
    async with httpx.AsyncClient() as client:
        user = await bot.get_chat_member(message.chat.id, message.from_user.id)
        username = dict(user).get('user').get('username')
        achievements_user_result = await client.get(f'{HOST_NAME}/get-achieve/{username}')
        achievements_user = achievements_user_result.json()
        if achievements_user:
            answer = parse_json(achievements_user, 'name_ach', 'ach_price')
            await message.answer(answer, parse_mode='Markdown')
        else:
            await message.answer('К сожалению, у тебя пока нет достижений.')


@dp.message_handler(lambda message: message.text == "Все достижения команд")
async def b_without_puree(message: types.Message):
    async with httpx.AsyncClient() as client:
        achievements_user_result = await client.get(f'{HOST_NAME}/list_achieve_teams')
        achievements_team = achievements_user_result.json()
        answer = parse_json(achievements_team, 'name_ach_team', 'ach_price')
        await message.answer(answer, parse_mode='Markdown')



@dp.message_handler()
async def get_status(message: Message):
    await message.answer(message.text)


async def get_notifications(message: Message):

    now = datetime.now()
    if now.isoweekday() == 6:
        diff = abs(timedelta(days=2, hours=now.hour, minutes=now.minute) - timedelta(hours=9))
    elif now.isoweekday() == 7:
        diff = abs(timedelta(days=1, hours=now.hour, minutes=now.minute) - timedelta(hours=9))
    else:
        diff = abs(timedelta(hours=now.hour, minutes=now.minute) - timedelta(hours=9))
    when_to_call = loop.time() + diff.total_seconds()
    loop.call_at(when_to_call, my_callback, message)
    async with httpx.AsyncClient() as client:
        user = await bot.get_chat_member(message.chat.id, message.from_user.id)
        username = dict(user).get('username')
        achievements_result = await client.get(f'{HOST_NAME}/get_mes/{status}')
        achievements = achievements_result.json().get('text_mes')
    await message.answer(str(achievements))


def my_callback(message):
    asyncio.ensure_future(get_notifications(message))


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
